## [4.0.3] - 2024-11-21
### Changed
- AJAX parameters

## [4.0.2] - 2024-11-21
### Added
- point not found exception

## [4.0.1] - 2024-11-21
### Fixed
- table creation

## [4.0.0] - 2024-11-20
### Added
- custom database table as points cache

## [3.0.0] - 2024-11-12
### Added
- pickup points search params

## [2.2.1] - 2024-03-08
### Fixed
- dynamic property

## [2.2.0] - 2022-05-22
### Added
- AJAX action

## [2.1.6] - 2022-06-17
### Fixed
- updating cache

## [2.1.5] - 2022-04-04
### Fixed
- error catching

## [2.1.3] - 2022-03-31
### Fixed
- translations

## [2.1.2] - 2022-03-31
### Changed
- refresh points only in cron
### Fixed
- error handling

## [2.1.0] - 2022-01-27
### Changed
- First schedule time
- Option name

## [2.0.1] - 2021-10-18
### Fixed
- Returned data during saving

## [2.0.0] - 2021-10-18
### Added
- Support for JSON data

## [1.1.0] - 2021-09-27
### Changed
- Allowed wpdesk/wp-builder ^2

## [1.0.1] - 2021-08-12
### Changed
- Translations

## [1.0.0] - 2021-08-09
### Added
- Init
