<?php
/**
 * Class LastRefreshTime
 *
 * @package WPDesk\PickupPoints
 */

namespace WPDesk\PickupPoints;

/**
 * Pickup Refresh data.
 */
class LastRefreshTime {

	const LAST_REFRESH_PICKUP_POINTS_TIME_FIELD = 'pickup_points_last_refresh_time_db';
	const LAST_REFRESH_PICKUP_POINTS_DATA_FIELD = 'pickup_points_last_refresh_data_db';
	const STATUS                                = 'status';
	const OK                                    = 'ok';
	const ERROR                                 = 'error';
	const MESSAGE                               = 'message';

	/**
	 * @var string
	 */
	private $service;

	/**
	 * @param string $service .
	 */
	public function __construct( string $service ) {
		$this->service = $service;
	}

	/**
	 * @return int
	 */
	public function get_last_refresh_time(): int {
		return (int) get_option( self::LAST_REFRESH_PICKUP_POINTS_TIME_FIELD . '_' . $this->service, 0 );
	}

	public function get_last_refresh_status(): string {
		$data = $this->get_last_refresh_status_data();

		return $data[ self::STATUS ] ?? self::OK;
	}

	public function get_last_refresh_message(): string {
		$data = $this->get_last_refresh_status_data();

		return $data[ self::MESSAGE ] ?? '';
	}

	private function get_last_refresh_status_data(): array {
		$data = get_option( self::LAST_REFRESH_PICKUP_POINTS_DATA_FIELD . '_' . $this->service, [] );
		$data = is_array( $data ) ? $data : [];

		return $data;
	}

}
