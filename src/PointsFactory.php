<?php

namespace WPDesk\PickupPoints;

use WPDesk\PickupPoints\Db\PickupPointCreatorFromDb;

class PointsFactory implements PickupPointCreatorFromDb {

	public function create_point_from_db( array $point_db_data ): Point {
		return new PointImplementation( $point_db_data );
	}

}
