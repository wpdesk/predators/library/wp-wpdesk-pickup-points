<?php

namespace WPDesk\PickupPoints;

use WPDesk\PickupPoints\Db\DbManager;
use WPDesk\PickupPoints\Db\PointNotFoundException;

class PickupPointsManagerImplementation implements PickupPointsManager
{

	protected DbManager $pickup_points_db_manager;

	/**
	 * @param DbManager $pickup_points_db_manager .
	 */
	public function __construct( DbManager $pickup_points_db_manager ) {
		$this->pickup_points_db_manager = $pickup_points_db_manager;
	}

	public function search_points( string $query, array $search_params = [] ): iterable {
		return $this->pickup_points_db_manager->search_points( $query, $search_params );
	}

	/**
	 * @param string $point_id
	 *
	 * @return Point
	 * @throws PointNotFoundException
	 */
	public function get_point_details( string $point_id ): Point {
		return $this->pickup_points_db_manager->get_point_by_point_id( $point_id );
	}

	public function refresh_points(): void {
		$this->pickup_points_db_manager->get_data( true );
	}

}
