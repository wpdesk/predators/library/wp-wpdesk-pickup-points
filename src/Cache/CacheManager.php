<?php
/**
 * Class CacheManager
 *
 * @package WPDesk\PickupPoints\Cache
 */

namespace WPDesk\PickupPoints\Cache;

use WPDesk\PickupPoints\Db\ObjectDataInterface;

/**
 * Cache manager.
 */
class CacheManager {
	const TIMEOUT_SUFFIX     = '_timeout';
	const AUTOLOAD_VALUE     = 'no';
	const OPTION_NAME_PREFIX = 'pickup_points_data_';

	/**
	 * @var ObjectDataInterface
	 */
	private $object_data;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var int
	 */
	private $expiration;

	/**
	 * @param ObjectDataInterface $object_data .
	 * @param string|null         $name        .
	 * @param int                 $expiration  .
	 */
	public function __construct( ObjectDataInterface $object_data, string $name = null, int $expiration = 86400 ) {
		$this->object_data = $object_data;
		$this->name        = $name ?: get_class( $object_data );
		$this->expiration  = $expiration;
	}

	/**
	 * @return mixed
	 * @throws GetDataException
	 */
	public function get_data( bool $force = false, $default_value = null ) {
		$cached_data = $this->get_cached_data();

		if ( ! $force && $cached_data !== null ) {
			return $cached_data;
		}

		try {
			return $this->set_cached_data( $this->object_data->get() );
		} catch ( \Throwable $e ) {
			if ( $force ) {
				throw $e;
			}
		}

		return $cached_data ?: $default_value;
	}

	/**
	 * @return void
	 */
	public function clear_cache() {
		delete_option( $this->get_option_name() );
		delete_option( $this->get_option_name_timeout() );
	}

	/**
	 * @return int
	 * @codeCoverageIgnore
	 */
	protected function get_current_time() {
		return time();
	}

	/**
	 * @return bool
	 */
	private function is_expired(): bool {
		$expiration = (int) get_option( $this->get_option_name_timeout(), 0 );

		return $this->get_current_time() > $expiration;
	}

	/**
	 * @return mixed
	 */
	private function get_cached_data() {
		$data = get_option( $this->get_option_name(), null );

		if ( is_string( $data ) ) {
			$json_data = json_decode( $data, true );

			if ( json_last_error() === JSON_ERROR_NONE ) {
				return $json_data;
			}
		}

		return $data;
	}

	/**
	 * @param mixed $cached_data .
	 *
	 * @return mixed
	 * @throws GetDataException
	 */
	private function set_cached_data( $cached_data ) {
		global $wpdb;
		$wpdb->show_errors();
		update_option( $this->get_option_name_timeout(), $this->get_current_time() + $this->expiration, self::AUTOLOAD_VALUE );

		$encoded_cached_data = json_encode( $cached_data, JSON_UNESCAPED_UNICODE );
		if ( $encoded_cached_data !== get_option( $this->get_option_name(), '' ) ) {
			if ( false === update_option( $this->get_option_name(), $encoded_cached_data, self::AUTOLOAD_VALUE ) ) {
				if ( $wpdb->last_error ) {
					throw new GetDataException( $wpdb->last_error );
				}
				throw new GetDataException( __( 'Unknown error while updating option.', 'wp-wpdesk-pickup-points' ) );
			}
		}

		return $cached_data;
	}

	/**
	 * @return string
	 */
	private function get_option_name() {
		return self::OPTION_NAME_PREFIX . $this->name;
	}

	/**
	 * @return string
	 */
	private function get_option_name_timeout() {
		return $this->get_option_name() . self::TIMEOUT_SUFFIX;
	}

}
