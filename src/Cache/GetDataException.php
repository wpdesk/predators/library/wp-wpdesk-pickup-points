<?php
/**
 * Class GetDataException
 *
 * @package WPDesk\PickupPoints\Cache
 */

namespace WPDesk\PickupPoints\Cache;

use RuntimeException;

/**
 * Exception get data.
 */
class GetDataException extends RuntimeException {

}
