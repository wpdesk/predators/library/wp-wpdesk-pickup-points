<?php
/**
 * Class PickupPoints
 *
 * @package WPDesk\PickupPoints
 */

namespace WPDesk\PickupPoints;

use Psr\Log\LoggerInterface;
use WPDesk\PluginBuilder\Plugin\HookableCollection;
use WPDesk\PluginBuilder\Plugin\HookableParent;

/**
 * Main library class.
 */
class PickupPoints implements HookableCollection {
	use HookableParent;

	/**
	 * @var string
	 */
	private $service;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @param string          $service .
	 * @param LoggerInterface $logger  .
	 */
	public function __construct( string $service, LoggerInterface $logger ) {
		$this->service = $service;
		$this->logger  = $logger;
	}

	/**
	 * @return void
	 */
	public function hooks() {
		$refresh_action = new RefreshAction( $this->service, $this->logger );

		$this->add_hookable( new RegisterCron( $this->service ) );
		$this->add_hookable( new CronAction( $this->service, $refresh_action ) );
		$this->add_hookable( new ManualAction( $this->service, $refresh_action ) );

		$this->hooks_on_hookable_objects();
	}
}
