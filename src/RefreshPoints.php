<?php

namespace WPDesk\PickupPoints;

use WPDesk\PickupPoints\Db\DbManager;
use WPDesk\PluginBuilder\Plugin\Hookable;

class RefreshPoints implements Hookable
{

	private string $service;

	private DbManager $pickup_points_db_manager;

	public function __construct( string $service, DbManager $pickup_points ) {
		$this->service                  = $service;
		$this->pickup_points_db_manager = $pickup_points;
	}

	public function hooks(): void {
		add_action( 'pickup-points/refresh/' . $this->service, [ $this, 'run_refresh' ] );
	}

	public function run_refresh(): void {
		$this->pickup_points_db_manager->get_data( true );
	}

}
