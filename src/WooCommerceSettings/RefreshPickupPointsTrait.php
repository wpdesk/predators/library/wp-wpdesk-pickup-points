<?php
/**
 * Trait RefreshPickupPointsTrait
 *
 * @package WPDesk\PickupPoints\WooCommerceSettings
 */

namespace WPDesk\PickupPoints\WooCommerceSettings;


use WPDesk\PickupPoints\WooCommerceSettings\Fields\RefreshPickupPointsField;

/**
 * Helper for rendering field.
 */
trait RefreshPickupPointsTrait {
	/**
	 * @param string $key  .
	 * @param array  $data .
	 *
	 * @return string
	 */
	public function generate_refresh_pickup_points_html( string $key, array $data ): string {
		return ( new RefreshPickupPointsField( $data ) )->render();
	}
}
