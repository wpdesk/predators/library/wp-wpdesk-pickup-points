<?php
/**
 * @var array  $data                      .
 * @var string $last_refresh_time_message .
 * @var string $refresh_url               .
 * @var bool  $error                      .
 * @var string $last_error_message        .
 */

$label_value = wp_kses_post( $data['title'] );

if ( $data['desc_trip'] ) {
	$label_value .= wc_help_tip( $data['desc_trip'] );
}
?>
<tr valign="top">
	<th scope="row" class="titledesc">
		<label><?php echo wp_kses_post( $label_value ); ?></label>
	</th>
	<td class="forminp">
		<a href="<?php echo esc_url( $refresh_url ); ?>"
		   class="button-secondary"><?php echo wp_kses_post( $data['label'] ); ?></a>

		<p class="description"><?php echo wp_kses_post( $last_refresh_time_message ); ?></p>
		<?php if ( $error ) : ?>
			<p class="description"><?php echo esc_html__( 'Recent refresh error:', 'wp-wpdesk-pickup-points' ); ?> <strong style="color: red;"><?php echo wp_kses_post( $last_error_message ); ?></strong></p>
		<?php endif; ?>
	</td>
</tr>
