<?php
/**
 * Trait RefreshPickupPointsTrait
 *
 * @package WPDesk\PickupPoints\WooCommerceSettings\Fields
 */

namespace WPDesk\PickupPoints\WooCommerceSettings\Fields;

use WPDesk\PickupPoints\Cache\CacheManager;
use WPDesk\PickupPoints\ManualAction;
use WPDesk\PickupPoints\LastRefreshTime;

/**
 * Render refresh pickup points field.
 */
class RefreshPickupPointsField {
	/**
	 * @var array .
	 */
	private $data;

	/**
	 * @param array $data .
	 */
	public function __construct( array $data ) {
		$this->data = $data;
	}

	/**
	 * @return string
	 */
	public function render(): string {
		$data                      = $this->get_prepared_data();
		$refresh_data              = $this->get_refresh_data( $data['service'] );
		$last_refresh_time_message = $this->get_last_refresh_time_message( $refresh_data->get_last_refresh_time() );
		$refresh_url               = $this->get_refresh_pickup_points_url( $data['service'] );
		$error                     = $refresh_data->get_last_refresh_status() === LastRefreshTime::ERROR;
		$last_error_message        = $refresh_data->get_last_refresh_message();

		ob_start();

		include __DIR__ . '/views/html-settings-field-refresh-pickup-points.php';

		$output = ob_get_clean();

		return trim( $output ? $output : __( 'Error rendering field.', 'wp-wpdesk-pickup-points' ) );
	}

	/**
	 * @codeCoverageIgnore
	 *
	 * @param string $service .
	 *
	 * @return LastRefreshTime
	 */
	protected function get_refresh_data( string $service ): LastRefreshTime {
		return new LastRefreshTime( $service );
	}

	/**
	 * @return array
	 */
	private function get_prepared_data(): array {
		return wp_parse_args( $this->data, $this->get_defaults() );
	}

	/**
	 * @return string[]
	 */
	private function get_defaults(): array {
		return [
			'service'   => '',
			'title'     => __( 'Pickup points cache', 'wp-wpdesk-pickup-points' ),
			'label'     => __( 'Clear cache and refresh the list of pickup points', 'wp-wpdesk-pickup-points' ),
			'desc_trip' => __( 'The list of pickup points is automatically updated once a day from the API. Use the Cache button if you notice outdated pickup points in the list.', 'wp-wpdesk-pickup-points' ),
		];
	}

	/**
	 * @param int $last_refresh .
	 *
	 * @return string
	 */
	private function get_last_refresh_time_message( int $last_refresh ): string {
		if ( $last_refresh > 0 ) {
			$humann_time_diff = human_time_diff( time(), $last_refresh ) . ' ' . __( 'ago', 'wp-wpdesk-pickup-points' );
		} else {
			$humann_time_diff = __( 'Never', 'wp-wpdesk-pickup-points' );
		}

		return __( 'Recent refresh: ', 'wp-wpdesk-pickup-points' ) . sprintf( "%s%s%s.", '<strong>', $humann_time_diff, '</strong>' );
	}

	/**
	 * @param string $service .
	 *
	 * @return string
	 */
	private function get_refresh_pickup_points_url( string $service ): string {
		$url = admin_url( 'admin-post.php' );
		$url = add_query_arg( 'action', ManualAction::ACTION, $url );
		$url = add_query_arg( 'service', $service, $url );

		return wp_nonce_url( $url, ManualAction::NONCE );
	}
}
