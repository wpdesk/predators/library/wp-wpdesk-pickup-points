<?php

namespace WPDesk\PickupPoints;

use WPDesk\PickupPoints\Ajax\Ajax;

class Select2Script
{

	private Ajax $ajax;

	private string $field_id;

	private string $action;

	private string $ajax_url;

	public function __construct( Ajax $ajax, string $field_id, string $action, string $ajax_url ) {
		$this->ajax = $ajax;
		$this->field_id = $field_id;
		$this->action = $action;
		$this->ajax_url = $ajax_url;
	}

	public function get_script(): string {
		ob_start();
		$nonce = $this->ajax->create_nonce();
		$field_id = $this->field_id;
		$action = $this->action;
		$ajax_url =$this->ajax_url;
		include __DIR__ . '/views/select2-script.php';
		return ob_get_clean();
	}

}
