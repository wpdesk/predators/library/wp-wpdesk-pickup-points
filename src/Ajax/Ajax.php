<?php

namespace WPDesk\PickupPoints\Ajax;

use WPDesk\PickupPoints\PickupPointsManager;
use WPDesk\PickupPoints\Point;
use \check_ajax_referer;
use \sanitize_text_field;
use \wp_create_nonce;
use \wp_send_json;

class Ajax
{

	const AJAX_ACTION_SUFFIX = '_collection_point_ajax';

	/**
	 * @var PickupPointsManager
	 */
	private $pickup_points_manager;

	/**
	 * @var string
	 */
	private $integration;

	private array $search_params;

	/**
	 * Ajax constructor.
	 *
	 * @param PickupPointsManager $pickup_points_manager
	 * @param string $integration
	 * @param SearchParam[] $search_params
	 */
	public function __construct( PickupPointsManager $pickup_points_manager, string $integration, array $search_params = [] ) {
		$this->pickup_points_manager = $pickup_points_manager;
		$this->integration           = $integration;
		$this->search_params         = $search_params;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_action( 'wp_ajax_' . $this->get_ajax_action(), array( $this, 'ajax_collection_points' ) );
		add_action( 'wp_ajax_nopriv_' . $this->get_ajax_action(), array( $this, 'ajax_collection_points' ) );
	}

	public function get_ajax_action(): string {
		return $this->integration . self::AJAX_ACTION_SUFFIX;
	}

	/**
	 * Creates nonce.
	 *
	 * @return string
	 */
	public function create_nonce() {
		return wp_create_nonce( $this->get_ajax_action() );
	}

	/**
	 * @param array $collection_points .
	 *
	 * @return array
	 */
	private function convert_points_for_select2( $collection_points ) {
		$elements = array( 'items' );
		foreach ( $collection_points as $id => $text ) {
			$elements['items'][] = array( 'id' => $id, 'text' => $text );
		}

		return $elements;
	}

	/**
	 * Handles AJAX request.
	 *
	 * @internal
	 */
	public function ajax_collection_points() {
		check_ajax_referer( $this->get_ajax_action(), 'security' );

		$query = sanitize_text_field( $_REQUEST['query'] ?? $_REQUEST['search'] ?? '' );
		try {
			$collection_points = $this->convert_iterator_to_array(
				$this->pickup_points_manager->search_points( $query, $this->prepare_search_params() )
			);
		} catch ( \Exception $e ) {
			$collection_points = array( 'error' => sprintf( __( 'Error while creating pickup points list: %1$s', 'wp-wpdesk-pickup-points' ), $e->getMessage() ) );
		}

		wp_send_json( $this->convert_points_for_select2( $collection_points ) );
	}

	/**
	 * @param iterable $collection_points_iterable
	 *
	 * @return array<string, string>
	 */
	private function convert_iterator_to_array( iterable $collection_points_iterable ): array {
		$collection_points = [];
		/** @var Point $point */
		foreach ( $collection_points_iterable as $point ) {
			$collection_points[ $point->get_point_id() ] = $point->get_label_with_point_id();
		}
		return $collection_points;
	}

	private function prepare_search_params(): array {
		$points_search_params = [];

		foreach ( $this->search_params as $param ) {
			if ( isset( $_REQUEST[ $param->get_name() ] ) ) {
				$value = sanitize_text_field( wp_unslash( $_REQUEST[ $param->get_name() ] ) );
				if ( ! empty( $value ) ) {
					if ( $param->is_boolean() ) {
						if ( filter_var( $value, FILTER_VALIDATE_BOOLEAN ) ) {
							$points_search_params[ $param->get_name() ] = 1;
						}
					} else {
						$points_search_params[ $param->get_name() ] = $value;
					}
				}
			}
		}

		return $points_search_params;
	}

}
