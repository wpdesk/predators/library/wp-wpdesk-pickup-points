<?php

namespace WPDesk\PickupPoints\Ajax;

class SearchParam {

	private string $name;

	private string $type;

	public function __construct( string $name, string $type ) {
		$this->name = $name;
		$this->type = $type;
	}

	public function get_name(): string {
		return $this->name;
	}

	public function get_type(): string {
		return $this->type;
	}

	public function is_boolean(): bool {
		return 'boolean' === $this->type;
	}

}
