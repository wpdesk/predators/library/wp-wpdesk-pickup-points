<?php
/**
 * Class ManualAction
 *
 * @package WPDesk\PickupPoints
 */

namespace WPDesk\PickupPoints;

use WC_Admin_Settings;
use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Manual action for refresh
 */
class ManualAction implements Hookable {
	const ACTION = 'refresh-pickup-points';
	const SERVICE = 'service';
	const STATUS = 'status';

	const NONCE = 'security';

	/**
	 * @var string
	 */
	private $service;

	/**
	 * @var RefreshAction
	 */
	private $refresh_action;

	/**
	 * @param string        $service        .
	 * @param RefreshAction $refresh_action .
	 */
	public function __construct( string $service, RefreshAction $refresh_action ) {
		$this->service        = $service;
		$this->refresh_action = $refresh_action;
	}

	/**
	 * @return void
	 */
	public function hooks() {
		add_action( 'admin_init', [ $this, 'add_message' ] );
		add_action( 'admin_post_' . self::ACTION, [ $this, 'refresh_pickup_points' ] );
	}

	/**
	 * @return void
	 */
	public function add_message() {
		$status  = $this->filter_input( INPUT_GET, self::STATUS );
		$service = $this->filter_input( INPUT_GET, self::ACTION );

		if ( $service !== $this->service ) {
			return;
		}

		if ( 'success' === $status ) {
			$this->add_wc_settings_message( __( 'Clearing the cache and refreshing the list of reception points successfully completed.', 'wp-wpdesk-pickup-points' ) );
		} elseif ( 'error' === $status ) {
			$this->add_wc_settings_error( __( 'An error occurred while clearing the cache and refreshing the list of pickup points. Please try again.', 'wp-wpdesk-pickup-points' ) );
		}
	}

	/**
	 * @return void
	 */
	public function refresh_pickup_points() {
		check_admin_referer( self::NONCE );

		$service = $this->filter_input( INPUT_GET, self::SERVICE );

		if ( $service !== $this->service ) {
			return;
		}

		if ( $this->refresh_action->start() ) {
			wp_safe_redirect( $this->get_redirect_url( 'success' ) );
		} else {
			wp_safe_redirect( $this->get_redirect_url( 'error' ) );
		}

		$this->end_request();
	}

	/**
	 * @return void
	 * @codeCoverageIgnore
	 */
	protected function end_request() {
		die();
	}

	/**
	 * @param string $message_text .
	 *
	 * @return void
	 * @codeCoverageIgnore
	 */
	protected function add_wc_settings_message( string $message_text ) {
		WC_Admin_Settings::add_message( $message_text );
	}

	/**
	 * @param string $message_text .
	 *
	 * @return void
	 * @codeCoverageIgnore
	 */
	protected function add_wc_settings_error( string $message_text ) {
		WC_Admin_Settings::add_error( $message_text );
	}

	/**
	 * @param int    $type     .
	 * @param string $var_name .
	 *
	 * @return mixed
	 * @codeCoverageIgnore
	 */
	protected function filter_input( int $type, string $var_name ) {
		return filter_input( $type, $var_name );
	}

	/**
	 * @param string $status .
	 *
	 * @return string
	 */
	private function get_redirect_url( string $status ): string {
		return add_query_arg( [
			self::ACTION => $this->service,
			self::STATUS => $status,
		], wp_get_referer() );
	}
}
