<?php
/**
 * @var string $field_id
 * @var string $action
 * @var string $nonce
 * @var string $ajax_url
 */
?><script type="text/javascript">
	jQuery( function ( $ ) {
		const ajax_nonce = '<?php echo esc_attr($nonce); ?>';
		const $field = $('#<?php echo esc_attr($field_id); ?>');
		let field_data = {...$field.data()};
		let options = {
			ajax: {
				url: '<?php echo esc_attr($ajax_url); ?>',
				dataType: 'json',
				delay: 300,
				type: 'POST',
				data: function (params) {
					return {
						...field_data,
						action: '<?php echo esc_attr($action); ?>',
						query: params.term,
						security: ajax_nonce,
					};
				},
				processResults: function (data, params) {
					return {
						results: data.items,
					};
				},
				cache: true,
			},
			allowClear: true,
			minimumInputLength: 3,
			width: 'resolve',
			dropdownAutoWidth: true,
			dropdownParent: jQuery('body'),
			language: {
				inputTooShort: function (args) {
					var remainingChars = args.minimum - args.input.length;
					return '<?php echo esc_attr(__( 'Enter minimum % characters', 'wp-wpdesk-pickup-points' )); ?>'.replace('%', remainingChars);
				},
				loadingMore: function () {
					return '<?php echo esc_attr(__( 'Loading more...', 'wp-wpdesk-pickup-points' )); ?>'
				},
				noResults: function () {
					return '<?php echo esc_attr(__( 'No pickup points.', 'wp-wpdesk-pickup-points' )); ?>'
				},
				searching: function () {
					return '<?php echo esc_attr(__( 'Searching for pickup points...', 'wp-wpdesk-pickup-points' )); ?>'
				},
				errorLoading: function () {
					return '<?php echo esc_attr(__( 'Error while loading pickup points.', 'wp-wpdesk-pickup-points' )); ?>'
				},
			},
		};
		if ($.fn.selectWoo) {
			$field.selectWoo(options);
		} else if ($.fn.select2) {
			$field.select2(options);
		}
	} );
</script>
