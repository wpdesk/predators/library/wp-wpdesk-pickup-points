<?php

namespace WPDesk\PickupPoints;

interface Point {

	public function get_point_id(): string;

	public function get_type(): string;

	public function get_title(): string;

	public function get_street(): string;

	public function get_zipcode(): string;

	public function get_city(): string;

	public function get_country(): string;

	public function get_label(): string;

	public function get_label_with_point_id(): string;

	public function is_cod(): bool;

	public function is_closed(): bool;

	public function is_active(): bool;

}
