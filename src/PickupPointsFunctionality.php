<?php

namespace WPDesk\PickupPoints;

use Psr\Log\LoggerInterface;
use WPDesk\PickupPoints\Ajax\Ajax;
use WPDesk\PickupPoints\Ajax\SearchParam;
use WPDesk\PickupPoints\Db\DatabaseTable;
use WPDesk\PickupPoints\Db\DbField;
use WPDesk\PickupPoints\Db\DbManager;
use WPDesk\PickupPoints\Db\ObjectDataInterface;
use WPDesk\PickupPoints\Db\Queries;

class PickupPointsFunctionality
{

	private PickupPointsManager $pickup_points_manager;

	private Ajax $ajax;

	/**
	 * @param \wpdb $wpdb
	 * @param string $service
	 * @param ObjectDataInterface $object_data
	 * @param PointsFactory $pickup_point_creator_from_db
	 * @param LoggerInterface $logger
	 * @param DbField[] $additional_table_fields
	 * @param SearchParam[] $search_params
	 * @param int $db_version
	 */
	public function create_objects(
		\wpdb $wpdb,
		string $service,
		ObjectDataInterface $object_data,
		PointsFactory $pickup_point_creator_from_db,
		LoggerInterface $logger,
		array $additional_table_fields,
		array $search_params = [],
		$db_version = 1
	) {
		$database_table = new DatabaseTable( $wpdb, $service, $additional_table_fields, $db_version );
		$database_table->hooks();

		$queries = new Queries( $database_table, $pickup_point_creator_from_db, $wpdb );

		$db_manager = new DbManager( $object_data, $queries, $database_table );

		$this->pickup_points_manager = new PickupPointsManagerImplementation( $db_manager );

		$this->ajax = new Ajax( $this->pickup_points_manager, $service, $search_params );
		$this->ajax->hooks();

		( new RefreshPoints( $service, $db_manager) )->hooks();

		$refresh_action = new RefreshAction( $service, $logger );

		( new RegisterCron( $service ) )->hooks();
		( new CronAction( $service, $refresh_action ) )->hooks();
		( new ManualAction( $service, $refresh_action ) )->hooks();
	}

	public function get_pickup_points_manager(): PickupPointsManager {
		return $this->pickup_points_manager;
	}

	public function create_select2_script( string $field_id ): Select2Script {
		return new Select2Script( $this->ajax, $field_id, $this->ajax->get_ajax_action(), \admin_url( 'admin-ajax.php' ) );
	}

}
