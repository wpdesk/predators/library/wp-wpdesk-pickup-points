<?php
/**
 * Class RefreshAction
 *
 * @package WPDesk\PickupPoints
 */

namespace WPDesk\PickupPoints;

use Exception;
use Psr\Log\LoggerInterface;

/**
 * Refresh action.
 */
class RefreshAction {
	/**
	 * @var string
	 */
	private $service;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @param string          $service .
	 * @param LoggerInterface $logger  .
	 */
	public function __construct( string $service, LoggerInterface $logger ) {
		$this->service = $service;
		$this->logger  = $logger;
	}

	/**
	 * @return bool
	 */
	public function start(): bool {
		set_time_limit( 0 );

		$status_data = [ LastRefreshTime::STATUS => LastRefreshTime::OK ];
		try {
			$this->do_action();

			return true;
		} catch ( \Throwable $e ) {
			$status_data[ LastRefreshTime::STATUS ]  = LastRefreshTime::ERROR;
			$status_data[ LastRefreshTime::MESSAGE ] = $e->getMessage();
			$this->logger->error( $e->getMessage() );

			return false;
		} finally {
			update_option( LastRefreshTime::LAST_REFRESH_PICKUP_POINTS_TIME_FIELD . '_' . $this->service, current_time( 'timestamp', true ), 'no' );
			update_option( LastRefreshTime::LAST_REFRESH_PICKUP_POINTS_DATA_FIELD . '_' . $this->service, $status_data );
		}
	}

	/**
	 * @codeCoverageIgnore
	 */
	protected function do_action() {
		do_action( 'pickup-points/refresh/' . $this->service );
	}
}
