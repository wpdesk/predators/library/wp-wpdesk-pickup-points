<?php
/**
 * Class RegisterCron
 *
 * @package WPDesk\PickupPoints
 */

namespace WPDesk\PickupPoints;

use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Register cron action.
 */
class RegisterCron implements Hookable {

	const CRON_TIMEOUT = 60*60*24; // DAY_IN_SECONDS

	private string $service;

	public function __construct( string $service ) {
		$this->service = $service;
	}

	public function hooks(): void {
		add_action( 'init', [ $this, 'register_cron_action' ] );
	}

	public function register_cron_action(): void {
		if ( $this->can_register_cron() ) {
			\as_schedule_recurring_action( time() - self::CRON_TIMEOUT - 10, self::CRON_TIMEOUT, CronAction::HOOK_NAME, $this->get_cron_args() );
		}
	}

	private function can_register_cron(): bool {
		return function_exists( 'as_schedule_recurring_action' ) && ! \as_next_scheduled_action( CronAction::HOOK_NAME, $this->get_cron_args() );
	}

	/**
	 * @return array<string, string>
	 */
	private function get_cron_args(): array {
		return [
			'service' => $this->service,
		];
	}

}
