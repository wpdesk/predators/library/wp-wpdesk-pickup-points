<?php

namespace WPDesk\PickupPoints\Db;

interface PickupPointDbData
{

	public function get_point_db_data(): array;

}
