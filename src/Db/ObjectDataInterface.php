<?php
/**
 * Interface ObjectDataInterface
 *
 * @package WPDesk\PickupPoints\Cache
 */

namespace WPDesk\PickupPoints\Db;

use WPDesk\PickupPoints\Cache\GetDataException;

/**
 * Object data.
 */
interface ObjectDataInterface {

	public function get(): iterable;

}
