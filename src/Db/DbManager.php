<?php

namespace WPDesk\PickupPoints\Db;

use WPDesk\PickupPoints\Point;

class DbManager {

	private ObjectDataInterface $object_data;

	private Queries $queries;

	private DatabaseTable $database_table;

	public function __construct( ObjectDataInterface $object_data, Queries $queries, DatabaseTable $database_table ) {
		$this->object_data    = $object_data;
		$this->queries        = $queries;
		$this->database_table = $database_table;
	}

	public function get_data( bool $force = false, $default_value = null ): \Generator {
		if ( $force ) {
			$this->database_table->create_table_when_needed( true );
			$this->queries->replace_points( $this->object_data );
		}

		return $this->queries->get_all_points();
	}

	public function search_points( string $search_query, array $conditions = [] ): \Generator {
		return $this->queries->search_points( $search_query, $conditions );
	}

	/**
	 * @param string $point_id
	 *
	 * @return Point
	 * @throws PointNotFoundException
	 */
	public function get_point_by_point_id( string $point_id ): Point {
		return $this->queries->get_point_by_point_id( $point_id );
	}

}
