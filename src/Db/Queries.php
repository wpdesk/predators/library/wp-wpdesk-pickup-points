<?php

namespace WPDesk\PickupPoints\Db;

use WPDesk\PickupPoints\Cache\GetDataException;
use WPDesk\PickupPoints\Point;

class Queries {

	private \wpdb $wpdb;

	private string $table_name;

	private PickupPointCreatorFromDb $creator_from_db;

	public function __construct( DatabaseTable $table, PickupPointCreatorFromDb $creator_from_db, \wpdb $wpdb ) {
		$this->table_name      = $table->get_table_name();
		$this->creator_from_db = $creator_from_db;
		$this->wpdb            = $wpdb;
	}

	public function replace_point( PickupPointDbData $pickup_point_db_data ): int {
		$replace = $this->wpdb->replace( $this->table_name, $pickup_point_db_data->get_point_db_data() );
		if ( $replace === false ) {
			throw new \Exception( 'Failed to insert point: '.$this->wpdb->last_error );
		}

		return $replace;
	}

	/**
	 * @return Point[]
	 */
	public function get_all_points(): \Generator {
		$query = "SELECT * FROM $this->table_name WHERE closed = 0";
		foreach ( $this->wpdb->get_results( $query, ARRAY_A ) as $row ) {
			yield $this->creator_from_db->create_point_from_db( $row );
		}
	}

	public function search_points( string $search_query, array $conditions = [] ): \Generator {
		$query  = "SELECT * FROM $this->table_name WHERE active = 1 AND MATCH (title, street, city, zipcode) AGAINST (%s IN BOOLEAN MODE) AND closed = %d";
		$params = [ '*' . $search_query . '*', 0 ];
		foreach ( $conditions as $field => $value ) {
			$query    .= ' AND '.$field.' = %'.( is_numeric( $value ) ? 'd' : 's' );
			$params[] = $value;
		}
		$query .= ' ORDER BY city, street';
		$query = $this->wpdb->prepare( $query, $params );
		foreach ( $this->wpdb->get_results( $query, ARRAY_A ) as $row ) {
			yield $this->creator_from_db->create_point_from_db( $row );
		}
	}

	/**
	 * @param string $point_id
	 *
	 * @return Point
	 * @throws PointNotFoundException
	 */
	public function get_point_by_point_id( string $point_id ): ?Point {
		$query = "SELECT * FROM $this->table_name WHERE point_id = %s";
		$query = $this->wpdb->prepare( $query, [ $point_id ] );
		$row   = $this->wpdb->get_row( $query, ARRAY_A );
		if ( $row ) {
			return $this->creator_from_db->create_point_from_db( $row );
		}

		throw new PointNotFoundException();
	}

	/**
	 * @param iterable $pickup_points_db_data
	 *
	 * @throws \Exception
	 */
	public function replace_points( ObjectDataInterface $object_data ): void {
		$this->wpdb->query( 'START TRANSACTION' );
		try {
			$this->wpdb->query( "UPDATE $this->table_name SET closed = 1" );
			foreach ( $object_data->get() as $pickup_point_db_data ) {
				$this->replace_point( $pickup_point_db_data );
			}
			$this->wpdb->query( 'COMMIT' );
		} catch ( \Exception $e ) {
			$this->wpdb->query( 'ROLLBACK' );
			throw $e;
		}
	}

}
