<?php

namespace WPDesk\PickupPoints\Db;

use WPDesk\PickupPoints\Point;
use WPDesk\PluginBuilder\Plugin\Hookable;

class DatabaseTable implements Hookable {

	private const DB_VERSION = '8';

	private string $table_name;

	private \wpdb $wpdb;

	private string $table_suffix;

	private string $db_version;

	/**
	 * @var DBField[]
	 */
	private array $additional_fields = [];

	public function __construct( \wpdb $wpdb, string $table_suffix, array $additional_fields = [], $db_version = '1' ) {
		$this->wpdb              = $wpdb;
		$this->table_name        = $wpdb->prefix.'points_'.$table_suffix;
		$this->table_suffix      = $table_suffix;
		$this->additional_fields = $additional_fields;
		$this->db_version        = self::DB_VERSION.'.'.$db_version;
	}

	public function hooks(): void {
		add_action( 'admin_init', [ $this, 'create_table_when_needed' ] );
	}

	public function create_table_when_needed( $force = false ): void {
		$option_name = $this->table_name.'_db_version';
		if ( ! $force && \get_option( $option_name, '0' ) === $this->db_version ) {
			return;
		}
		$this->delete_old_cache_options();

		$additional_fields = $this->prapare_additional_fields_sql();

		$charset_collate = $this->wpdb->get_charset_collate();
		$sql             = "CREATE TABLE $this->table_name (
			id int(11) NOT NULL AUTO_INCREMENT,
			closed tinyint(1) NOT NULL DEFAULT 0,
			active tinyint(1) NOT NULL DEFAULT 1,
			point_id varchar(125) NOT NULL,
			type varchar(125) NOT NULL,
			title varchar(125) NOT NULL,
			street varchar(125) NOT NULL,
			zipcode varchar(20) NOT NULL,
			city varchar(50) NOT NULL,
			country varchar(20) NOT NULL,
			cod tinyint(1) NOT NULL DEFAULT 0,
			meta_data longtext,
			PRIMARY KEY id (id),
			UNIQUE KEY point_id (point_id),
			INDEX active (active),
			INDEX cod (cod),
			INDEX country (country),
			FULLTEXT KEY fuultext_index (title, street, zipcode, city)$additional_fields
		) $charset_collate;";
		require_once ABSPATH.'wp-admin/includes/upgrade.php';
		\dbDelta( $sql );
		\update_option( $option_name, self::DB_VERSION );
	}

	private function prapare_additional_fields_sql(): string {
		$additional_fields_sql = '';
		foreach ( $this->additional_fields as $field ) {
			$additional_fields_sql .= ",\n\t\t\t{$field->get_create_table_sql()}";
			$additional_fields_sql .= ",\n\t\t\tINDEX {$field->get_name()} ({$field->get_name()})";
		}

		return $additional_fields_sql;
	}

	private function delete_old_cache_options(): void {
		\delete_option( 'pickup_points_data_'.$this->table_suffix );
		\delete_option( 'pickup_points_data_'.$this->table_suffix.'_timeout' );
	}

	public function get_table_name(): string {
		return $this->table_name;
	}

}
