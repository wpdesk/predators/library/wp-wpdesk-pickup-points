<?php

namespace WPDesk\PickupPoints\Db;

use WPDesk\PickupPoints\Point;

interface PickupPointCreatorFromDb
{
	public function create_point_from_db(array $point_db_data): Point;

}
