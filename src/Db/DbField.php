<?php

namespace WPDesk\PickupPoints\Db;

class DbField {

	private string $name;

	private string $create_table_sql;

	public function __construct( string $name, string $create_table_sql ) {
		$this->name             = $name;
		$this->create_table_sql = $create_table_sql;
	}

	public function get_name(): string {
		return $this->name;
	}

	public function get_create_table_sql(): string {
		return $this->create_table_sql;
	}

}
