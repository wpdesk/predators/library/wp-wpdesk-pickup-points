<?php

namespace WPDesk\PickupPoints;

use WPDesk\PickupPoints\Db\PickupPointDbData;

class PointImplementation implements Point, PickupPointDbData {

	private const         POINT_ID = 'point_id';
	private const         TYPE     = 'type';
	private const         TITLE    = 'title';
	private const         STREET   = 'street';
	private const         ZIPCODE  = 'zipcode';
	private const         CITY     = 'city';
	private const         COUNTRY  = 'country';
	private const         COD      = 'cod';
	private const         CLOSED   = 'closed';
	private const         ACTIVE   = 'active';

	private array $point_data;

	public function __construct( array $point_data = [] ) {
		$defaults         = [
			self::ACTIVE   => 1,
		];
		$this->point_data = array_merge( $defaults, $point_data );
	}

	public function get_point_id(): string {
		return $this->point_data[ self::POINT_ID ] ?? '';
	}

	public function set_point_id( string $point_id ): void {
		$this->point_data[ self::POINT_ID ] = $point_id;
	}

	public function get_type(): string {
		return $this->point_data[ self::TYPE ] ?? '';
	}

	public function set_type( string $type ): void {
		$this->point_data[ self::TYPE ] = $type;
	}

	public function get_title(): string {
		return $this->point_data[ self::TITLE ] ?? '';
	}

	public function set_title( string $title ): void {
		$this->point_data[ self::TITLE ] = $title;
	}

	public function get_street(): string {
		return $this->point_data[ self::STREET ] ?? '';
	}

	public function set_street( string $street ): void {
		$this->point_data[ self::STREET ] = $street;
	}

	public function get_zipcode(): string {
		return $this->point_data[ self::ZIPCODE ] ?? '';
	}

	public function set_zipcode( string $zipcode ): void {
		$this->point_data[ self::ZIPCODE ] = $zipcode;
	}

	public function get_city(): string {
		return $this->point_data[ self::CITY ] ?? '';
	}

	public function set_city( string $city ): void {
		$this->point_data[ self::CITY ] = $city;
	}

	public function get_country(): string {
		return $this->point_data[ self::COUNTRY ] ?? '';
	}

	public function set_country( string $country ): void {
		$this->point_data[ self::COUNTRY ] = $country;
	}

	public function get_label(): string {
		return $this->get_street().', '.$this->get_zipcode().' '.$this->get_city();
	}

	public function get_label_with_point_id(): string {
		return $this->get_point_id().' - '.$this->get_label();
	}

	public function is_cod(): bool {
		return $this->point_data[ self::COD ] === 1;
	}

	public function set_cod( bool $cod ): void {
		$this->point_data[ self::COD ] = $cod ? 1 : 0;
	}

	public function is_closed(): bool {
		return $this->point_data[ self::CLOSED ] === 1;
	}

	public function set_closed( bool $closed ): void {
		$this->point_data[ self::CLOSED ] = $closed ? 1 : 0;
	}

	public function is_active(): bool {
		return $this->point_data[ self::ACTIVE ] === 1;
	}

	public function set_active( bool $active ): void {
		$this->point_data[ self::ACTIVE ] = $active ? 1 : 0;
	}

	public function get_point_db_data(): array {
		return $this->point_data;
	}

	protected function get_point_field_string( string $field ): string {
		return $this->point_data[ $field ];
	}

	protected function set_point_field_string( string $field, string $value ): void {
		$this->point_data[ $field ] = $value;
	}

	protected function get_point_field_bool( string $field ): bool {
		return $this->point_data[ $field ] === 1;
	}

	protected function set_point_field_bool( string $field, bool $value ): void {
		$this->point_data[ $field ] = $value ? 1 : 0;
	}

	protected function get_point_field_int( string $field ): int {
		return $this->point_data[ $field ];
	}

	protected function set_point_field_int( string $field, int $value ): void {
		$this->point_data[ $field ] = $value;
	}

	public function get_point_data(): array {
		return $this->point_data;
	}

}
