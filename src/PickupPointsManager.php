<?php

namespace WPDesk\PickupPoints;

interface PickupPointsManager
{

	public function search_points( string $query, array $search_params = [] ): iterable;

	public function get_point_details( string $point_id ): ?Point;

	public function refresh_points(): void;

}
