<?php
/**
 * Class CronAction
 *
 * @package WPDesk\PickupPoints
 */

namespace WPDesk\PickupPoints;

use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Cron action refresh.
 */
class CronAction implements Hookable {
	const HOOK_NAME = 'refresh_pickup_points';

	/**
	 * @var string
	 */
	private $service;

	/**
	 * @var RefreshAction
	 */
	private $refresh_action;

	/**
	 * @param string        $service        .
	 * @param RefreshAction $refresh_action .
	 */
	public function __construct( string $service, RefreshAction $refresh_action ) {
		$this->service        = $service;
		$this->refresh_action = $refresh_action;
	}

	/**
	 * @return void
	 */
	public function hooks() {
		add_action( self::HOOK_NAME, [ $this, 'refresh_pickup_points' ] );
	}

	/**
	 * @param string $service .
	 *
	 * @return void
	 */
	public function refresh_pickup_points( string $service ) {
		set_time_limit( 0 );

		if ( $this->service !== $service ) {
			return;
		}

		$this->refresh_action->start();
	}
}
