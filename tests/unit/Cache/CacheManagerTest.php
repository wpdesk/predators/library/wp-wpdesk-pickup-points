<?php

namespace unit;

use PHPUnit\Framework\MockObject\MockObject;
use WP_Mock;
use WP_Mock\Tools\TestCase;
use WPDesk\PickupPoints\Cache\CacheManager;
use WPDesk\PickupPoints\Cache\GetDataException;
use WPDesk\PickupPoints\Db\ObjectDataInterface;


class CacheManagerTest extends TestCase {
	/**
	 * @var MockObject|ObjectDataInterface
	 */
	private $object_data;
	/**
	 * @var MockObject|ObjectDataInterface
	 */
	private $cache_manager_under_test;

	public function setUp(): void {
		WP_Mock::setUp();

		$this->object_data = $this->getMockBuilder( ObjectDataInterface::class )->setMethods( [ 'get' ] )->getMock();

		$this->cache_manager_under_test = $this->getMockBuilder( CacheManager::class )
		                                       ->setConstructorArgs( [ $this->object_data, 'cache_name' ] )
		                                       ->setMethods( [ 'get_current_time' ] )->getMock();
	}

	public function tearDown(): void {
		WP_Mock::tearDown();
	}

	public function testShouldThrowExceptionIfObjectDataThrowException() {
		// Except.
		$exception = new GetDataException();

		$this->expectException( GetDataException::class );
		$this->object_data->expects( self::once() )->method( 'get' )->willThrowException( $exception );

		WP_Mock::userFunction( 'get_option' )->once()->withArgs( [ 'pickup_points_data_cache_name', null ] )->andReturnNull();


		// Given.

		// When.
		$actual = $this->cache_manager_under_test->get_data( true );

		// Then.
		$this->assertTrue( true );
	}

	public function testShouldGetAndSaveIfCacheIsNull() {
		// Except.
		$expected = [ 'data to cache' ];

		$this->object_data->expects( self::once() )->method( 'get' )->willReturn( $expected );
		$this->cache_manager_under_test->expects( self::once() )->method( 'get_current_time' )->willReturn( 1 );

		WP_Mock::userFunction( 'get_option' )->twice()->withArgs( [ 'pickup_points_data_cache_name', null ] )->andReturnNull();
		WP_Mock::userFunction( 'update_option' )->once()->with( 'pickup_points_data_cache_name', json_encode( $expected, JSON_UNESCAPED_UNICODE ), 'no' )->andReturnTrue();
		WP_Mock::userFunction( 'update_option' )->once()->with( 'pickup_points_data_cache_name_timeout', 86401, 'no' )->andReturnTrue();

		global $wpdb;
		$wpdb = $this->getMockBuilder( 'wpdb' )->setMethods( [ 'show_errors' ] )->getMock();
		$wpdb->expects( self::once() )->method( 'show_errors' )->willReturn( true );

		// When.
		$actual = $this->cache_manager_under_test->get_data();

		// Then.
		$this->assertEquals( $expected, $actual );
	}

	public function testShouldGetSavedDataIfNotExpired() {
		// Except

		WP_Mock::userFunction( 'get_option' )->once()->with( 'pickup_points_data_cache_name', null )->andReturn( json_encode( [ 'cached data' ], JSON_UNESCAPED_UNICODE ) );
		WP_Mock::userFunction( 'get_option' )->never()->with( 'pickup_points_data_cache_name_timeout', 0 )->andReturn( 2 );

		// When.
		$actual = $this->cache_manager_under_test->get_data();

		// Then.
		$this->assertEquals( [ 'cached data' ], $actual );
	}

	public function testShouldGetSavedData() {
		// Except.
		$expected = [ 'cached data' ];

		WP_Mock::userFunction( 'get_option' )->once()->with( 'pickup_points_data_cache_name', null )->andReturn( json_encode( $expected ) );

		// When.
		$actual = $this->cache_manager_under_test->get_data();

		// Then.
		$this->assertEquals( $expected, $actual );
	}

	public function testShouldGetDefaultValueIfThrowNewException() {
		// Except.
		$exception = new GetDataException;
		$this->object_data->expects( self::once() )->method( 'get' )->willThrowException( $exception );

		WP_Mock::userFunction( 'get_option' )->once()->with( 'pickup_points_data_cache_name', null )->andReturnNull();

		// When.
		$actual = $this->cache_manager_under_test->get_data( false, 'default_value' );

		// Then.
		$this->assertEquals( 'default_value', $actual );
	}

	public function testShouldClearCache() {
		// Except.
		WP_Mock::userFunction( 'delete_option' )->once()->with( 'pickup_points_data_cache_name' );
		WP_Mock::userFunction( 'delete_option' )->once()->with( 'pickup_points_data_cache_name_timeout' );

		// When.
		$this->cache_manager_under_test->clear_cache();

		// Then.
		$this->assertTrue( true );
	}

	public function testShouldGetSavedDataIfIsArray() {
		// Except.
		$expected = [ 'array_of_data' ];

		$this->object_data->expects( self::never() )->method( 'get' );

		WP_Mock::userFunction( 'get_option' )->once()->withArgs( [ 'pickup_points_data_cache_name', null ] )->andReturn( $expected );

		// When.
		$actual = $this->cache_manager_under_test->get_data();

		// Then.
		$this->assertEquals( $expected, $actual );
	}

	public function testShouldGetSavedDataIfIsStringAndNotIsJSON() {
		// Except.
		$expected = 'string of data';

		$this->object_data->expects( self::never() )->method( 'get' );

		WP_Mock::userFunction( 'get_option' )->once()->withArgs( [ 'pickup_points_data_cache_name', null ] )->andReturn( $expected );

		// When.
		$actual = $this->cache_manager_under_test->get_data();

		// Then.
		$this->assertEquals( $expected, $actual );
	}

	public function testShouldGetSavedDataIfIsStringAndIsJSON() {
		// Except.
		$this->object_data->expects( self::never() )->method( 'get' );

		WP_Mock::userFunction( 'get_option' )->once()->withArgs( [ 'pickup_points_data_cache_name', null ] )->andReturn( '["json data"]' );

		// When.
		$actual = $this->cache_manager_under_test->get_data();

		// Then.
		$this->assertEquals( [ 'json data' ], $actual );
	}
}
