[![pipeline status](https://gitlab.com/wpdesk/predators/library/wp-wpdesk-pickup-points/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/library/wp-wpdesk-activation-reminder/commits/master)
Integration: [![coverage report](https://gitlab.com/wpdesk/predators/library/wp-wpdesk-pickup-points/badges/master/coverage.svg?job=integration+test+lastest+coverage)](https://gitlab.com/wpdesk/library/wp-wpdesk-activation-reminder/commits/master)
Unit: [![coverage report](https://gitlab.com/wpdesk/predators/library/wp-wpdesk-pickup-points/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/library/wp-wpdesk-activation-reminder/commits/master)

# wp-wpdesk-pickup-points

## Usage

```bash
composer require --dev wpdesk/wp-wpdesk-pickup-points

composer update
```